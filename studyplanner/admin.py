from django.contrib import admin
from .models import Semester, Module, Coursework, Task, Milestone

# Register your models here.

admin.site.register(Semester)
admin.site.register(Module)
admin.site.register(Coursework)
admin.site.register(Task)
admin.site.register(Milestone)