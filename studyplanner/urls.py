from django.conf.urls import url
from django.urls import path

from studyplanner import views

app_name = 'studyplanner'
urlpatterns = [
    # /studyplanner/
    path('', views.IndexView.as_view(), name='index'),

    # /studyplanner/m_id/cw_id/
    path('<int:module_id>/<int:coursework_id>/', views.add_task, name='task_new'),

    # /studyplanner/m_id/cw_id/t_id/
    path('<int:module_id>/edit/<int:task_id>/', views.edit_task, name='edit_task'),

    # /studyplanner/id/
    # (id,method controller,name of template)
    path('<int:module_id>/', views.module_details, name='detail'),

     path('milestone/<int:module_id>/',views.milestone_new,name='milestone_new'),

]