
from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from django.contrib.auth.models import User
from django.forms import DateField

from .models import Task, Milestone

from bootstrap_datepicker_plus import DatePickerInput

class TaskModelForm(forms.ModelForm):
    class Meta:
        model = Task
        fields=[
            'task_name','date_start','date_due','time_that_will_be_spent','task_activity',
            'criterion_type','requirement_criterion', 'notes',
        ]
        widgets = {
            'date_start': DatePickerInput(format='%Y-%m-%d'),  # specify date-frmat

            'date_due': DatePickerInput(format='%Y-%m-%d'),  # specify date-frmat
        }


class MilestoneModelForm(forms.ModelForm):
    class Meta:
        model = Milestone

        fields=[
            'milestone_name','set_date','end_date',
        ]

        widgets = {
            'set_date' : DatePickerInput(format='%Y-%m-%d'),
            'end_date': DatePickerInput(format='%Y-%m-%d'),
        }
